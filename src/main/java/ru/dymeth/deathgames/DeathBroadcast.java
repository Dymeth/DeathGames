package ru.dymeth.deathgames;

import com.google.common.collect.Iterables;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.*;

public class DeathBroadcast implements PluginMessageListener {
    private final DeathGames plugin;
    private final String channel;

    public DeathBroadcast(DeathGames plugin, String channel) {
        this.plugin = plugin;
        this.channel = channel;
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!this.channel.equals(channel)) {
            return;
        }

        ByteArrayInputStream stream = new ByteArrayInputStream(message);
        DataInputStream in = new DataInputStream(stream);
        try {
            if (in.readUTF().equals("getInfo")) {
                sendMessage(this.plugin.getArena().getState().toString()
                        + ";" + Bukkit.getOnlinePlayers().size()
                        + ";" + this.plugin.getArena().getMaxStartPlayers()
                );
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String message) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        try {
            out.writeUTF(this.channel);
            out.writeUTF(message);
        } catch (IOException e) {
            DeathLogger.info("Не удалось записать сообщение для BroadCast");
            e.printStackTrace();
        }

        Player sender = Iterables.getFirst(this.plugin.getServer().getOnlinePlayers(), null);
        if (sender != null) sender.sendPluginMessage(this.plugin, "BungeeCord", stream.toByteArray());
    }
}
