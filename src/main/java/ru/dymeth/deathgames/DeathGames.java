package ru.dymeth.deathgames;

import com.destroystokyo.paper.PaperConfig;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;
import org.spigotmc.SpigotConfig;
import ru.dymeth.deathgames.arena.GameDeathArena;
import ru.dymeth.deathgames.economy.EconomyManager;
import ru.dymeth.deathgames.item.ItemManager;
import ru.dymeth.deathgames.listener.ArenaListener;
import ru.dymeth.deathgames.listener.GameListener;
import ru.dymeth.deathgames.listener.LobbyListener;

public class DeathGames extends JavaPlugin {
    private GameDeathArena arena;
    private ItemManager itemManager;

    @Override
    public void onEnable() {
        DeathLogger.setLogger(this.getLogger());
        PaperConfig.savePlayerData = false;
        SpigotConfig.disableAdvancementSaving = true;
        SpigotConfig.disableStatSaving = true;

        this.saveDefaultConfig();
        ConfigurationSection arenaConfig = this.getConfig().getConfigurationSection("arena");

        this.itemManager = new ItemManager(this);
        EconomyManager economyManager = new EconomyManager(
                arenaConfig.getInt("winner-prize"),
                arenaConfig.getInt("loser-prize")
                );

        this.arena = new GameDeathArena(this,
                economyManager,
                arenaConfig.getInt("min-start-players"),
                arenaConfig.getInt("min-players-timer-seconds"),
                arenaConfig.getInt("max-start-players"),
                arenaConfig.getInt("max-players-timer-seconds"),
                arenaConfig.getInt("ending-duration-seconds"));
        Bukkit.getOnlinePlayers().forEach(player -> this.arena.addPlayer(player));
        Bukkit.getPluginManager().registerEvents(new ArenaListener(this), this);
        Bukkit.getPluginManager().registerEvents(new GameListener(this), this);
        Bukkit.getPluginManager().registerEvents(new LobbyListener(this), this);

        String channel = this.getName().toLowerCase() + ":broadcast";
        DeathBroadcast deathBroadcast = new DeathBroadcast(this, channel);
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        this.getServer().getMessenger().registerIncomingPluginChannel(this, channel, deathBroadcast);
    }

    @Override
    public void onDisable() {
        HandlerList.unregisterAll(this);
        if (this.arena != null) this.arena.forceUnload();
    }

    public GameDeathArena getArena() {
        return this.arena;
    }

    public ItemManager getItemManager() {
        return this.itemManager;
    }
}
