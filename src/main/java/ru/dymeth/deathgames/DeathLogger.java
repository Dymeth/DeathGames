package ru.dymeth.deathgames;

import java.util.logging.Logger;

public class DeathLogger {
    private static Logger handle;

    public static void setLogger(Logger logger) {
        if (handle != null) throw new IllegalStateException("Логгер уже задан");
        handle = logger;
    }

    public static void info(String message) {
        handle.info(message);
    }
}
