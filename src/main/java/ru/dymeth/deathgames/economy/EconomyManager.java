package ru.dymeth.deathgames.economy;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import ru.dymeth.flexdonate.economy.BalanceManager;
import ru.dymeth.flexdonate.economy.TmoneyBalanceManager;

import java.math.BigDecimal;

public class EconomyManager {
    private final int winnerPrize;
    private final int loserPrize;
    private final BalanceManager balanceManager;

    public EconomyManager(int winnerPrize, int loserPrize) {
        this.winnerPrize = winnerPrize;
        this.loserPrize = loserPrize;
        this.balanceManager = new TmoneyBalanceManager("DeathGamePoint",
                "очки", "очков", "очках", '$');
    }

    public void depositPrize(Iterable<Player> winners, Iterable<Player> losers) {
        winners.forEach(player -> {
            this.balanceManager.deposit(player, BigDecimal.valueOf(this.winnerPrize));
            player.sendMessage("Вы получили " + ChatColor.GREEN + this.winnerPrize + ChatColor.WHITE + " очков за победу");
            player.sendMessage("Ваш баланс " + ChatColor.GREEN + this.balanceManager.getBalance(player));
        });
        losers.forEach(player -> {
            this.balanceManager.deposit(player, BigDecimal.valueOf(this.loserPrize));
            player.sendMessage("Вы получили " + ChatColor.GREEN + this.loserPrize + ChatColor.WHITE + " очков за проигрыш");
            player.sendMessage("Ваш баланс " + ChatColor.GREEN + this.balanceManager.getBalance(player));
        });
    }
}
