package ru.dymeth.deathgames.event;

import org.bukkit.event.HandlerList;
import ru.dymeth.deathgames.arena.GameArena;

public class ArenaEverySecondTickEvent extends ArenaEvent {
    private static final HandlerList handlers = new HandlerList();

    public ArenaEverySecondTickEvent(GameArena arena) {
        super(arena);
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
