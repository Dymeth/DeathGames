package ru.dymeth.deathgames.event;

import org.bukkit.event.HandlerList;
import ru.dymeth.deathgames.arena.GameArena;

public final class ArenaStartEvent extends ArenaEvent {
    private static final HandlerList handlers = new HandlerList();

    public ArenaStartEvent(GameArena arena) {
        super(arena);
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
