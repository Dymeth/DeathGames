package ru.dymeth.deathgames.item;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.player.PlayerInteractEvent;
import ru.dymeth.deathgames.DeathGames;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public final class ItemManager {
    private final Map<String, UsableItem> usableItems = new HashMap<>();

    public ItemManager(DeathGames plugin) {
        this.usableItems.put("thunder_item", new UsableItem(1) {
            @Override
            public boolean onUse(PlayerInteractEvent event) {
                Block block;
                try {
                    block = event.getPlayer().getTargetBlock(null, 7);
                } catch (Exception e) {
                    return false;
                }
                if (block == null || block.getType() == Material.AIR) return false;
                Location targetLoc = block.getLocation().add(0.5, 0, 0.5);
                Bukkit.getScheduler().runTaskLater(plugin, () ->
                        targetLoc.getWorld().strikeLightning(targetLoc), this.getCooldownSeconds() * 20L);
                return true;
            }
        });
    }

    @Nullable
    public UsableItem getUsableItem(String id) {
        return this.usableItems.get(id);
    }
}
