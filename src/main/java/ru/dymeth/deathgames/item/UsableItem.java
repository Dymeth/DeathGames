package ru.dymeth.deathgames.item;

import org.bukkit.event.player.PlayerInteractEvent;

public abstract class UsableItem {
    private final int cooldownSeconds;

    public UsableItem(int cooldownSeconds) {
        this.cooldownSeconds = cooldownSeconds;
    }

    public int getCooldownSeconds() {
        return cooldownSeconds;
    }

    // Если вернуло true то предмет необходимо уничтожить, иначе не ломать =)
    public abstract boolean onUse(PlayerInteractEvent event);
}
