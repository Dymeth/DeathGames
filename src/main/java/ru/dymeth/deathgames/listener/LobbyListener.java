package ru.dymeth.deathgames.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import ru.dymeth.deathgames.DeathGames;
import ru.dymeth.deathgames.arena.GameDeathArena;
import ru.dymeth.deathgames.arena.GameState;

public class LobbyListener implements Listener {
    private final GameDeathArena arena;

    public LobbyListener(DeathGames plugin) {
        this.arena = plugin.getArena();
    }

    @EventHandler
    private void onFall(EntityDamageEvent event) {
        if (event.getCause() != EntityDamageEvent.DamageCause.VOID) return;
        if (this.arena.getState() == GameState.GAME) return;
        if (!(event.getEntity() instanceof Player)) return;
        Bukkit.getScheduler().runTask(this.arena.getPlugin(), () ->
                event.getEntity().teleport(this.arena.getSpawnLocation()));
    }

    @EventHandler
    private void on(EntityDamageEvent event) {
        this.cancelOnLobby(event);
    }

    @EventHandler
    private void on(FoodLevelChangeEvent event) {
        this.cancelOnLobby(event);
    }

    @EventHandler
    private void on(EntityTargetEvent event) {
        this.cancelOnLobby(event);
    }

    @EventHandler
    private void on(BlockBreakEvent event) {
        this.cancelOnLobby(event);
    }

    @EventHandler
    private void on(BlockPlaceEvent event) {
        this.cancelOnLobby(event);
    }

    @EventHandler
    private void on(PlayerInteractEvent event) {
        this.cancelOnLobby(event);
    }

    @EventHandler
    private void on(PlayerInteractEntityEvent event) {
        this.cancelOnLobby(event);
    }

    @EventHandler
    private void on(PlayerInteractAtEntityEvent event) {
        this.cancelOnLobby(event);
    }

    @EventHandler
    private void on(EntityPickupItemEvent event) {
        this.cancelOnLobby(event);
    }

    private void cancelOnLobby(Cancellable event) {
        if (this.arena.getState() != GameState.GAME) event.setCancelled(true);
    }
}
