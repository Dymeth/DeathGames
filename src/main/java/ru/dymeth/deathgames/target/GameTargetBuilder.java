package ru.dymeth.deathgames.target;

import org.bukkit.Location;
import org.bukkit.block.Biome;
import org.bukkit.entity.Entity;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;
import ru.dymeth.deathgames.arena.GameDeathArena;
import ru.dymeth.deathgames.item.ItemBuilder;
import ru.dymeth.deathgames.listener.CustomListener;
import ru.dymeth.deathgames.util.LocationUtils;
import ru.dymeth.deathgames.util.Randomizer;
import ru.dymeth.deathgames.util.RegularMob;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

public class GameTargetBuilder {
    private final int durationSeconds;
    private final boolean shouldDie;
    private final String victimMessage;
    private final String opponentsMessage;
    private Predicate<EntityDamageEvent> filter = null;
    private String world = "arena_normal";
    private Boolean night = null;
    private Boolean thunder = null;
    private final Map<ItemStack, Integer> victimKitCooldowns = new HashMap<>();
    private final Map<ItemStack, Integer> opponentsKitCooldowns = new HashMap<>();
    private Listener listener;
    private final Set<Biome> allowedBiomes = new HashSet<>();
    private final Set<RegularMob> regularMobs = new HashSet<>();

    public GameTargetBuilder(
            int durationSeconds,
            boolean shouldDie,
            String victimMessage,
            String opponentsMessage
    ) {
        this.durationSeconds = durationSeconds;
        this.shouldDie = shouldDie;
        this.victimMessage = victimMessage;
        this.opponentsMessage = opponentsMessage;
    }

    public GameTargetBuilder world(String world) {
        this.world = world;
        return this;
    }

    public GameTargetBuilder night() {
        return this.night(true);
    }

    public GameTargetBuilder night(boolean night) {
        this.night = night;
        return this;
    }

    public GameTargetBuilder thunder() {
        return this.thunder(true);
    }

    public GameTargetBuilder thunder(boolean thunder) {
        this.thunder = thunder;
        return this;
    }

    public GameTargetBuilder filter(EntityDamageEvent.DamageCause... causes) {
        if (this.filter != null) throw new IllegalStateException("Фильтр уже задан");
        this.filter = event -> {
            for (EntityDamageEvent.DamageCause cause : causes)
                if (event.getCause() == cause) return true;
            return false;
        };
        return this;
    }

    public GameTargetBuilder filter(Predicate<EntityDamageEvent> filter) {
        if (this.filter != null) throw new IllegalStateException("Фильтр уже задан");
        this.filter = filter;
        return this;
    }

    public GameTargetBuilder addToVictimStartKit(ItemBuilder... stacks) {
        return this.addToVictimKit(-1, stacks);
    }

    public GameTargetBuilder addToVictimKit(int cooldownSeconds, ItemBuilder... stacks) {
        for (ItemBuilder builder : stacks)
            this.victimKitCooldowns.put(builder.create(), cooldownSeconds);
        return this;
    }

    public GameTargetBuilder addToOpponentsStartKit(ItemBuilder... stacks) {
        return this.addToOpponentsKit(-1, stacks);
    }

    public GameTargetBuilder addToOpponentsKit(int cooldownSeconds, ItemBuilder... stacks) {
        for (ItemBuilder builder : stacks)
            this.opponentsKitCooldowns.put(builder.create(), cooldownSeconds);
        return this;
    }

    public GameTargetBuilder setListener(CustomListener listener) {
        this.listener = listener;
        return this;
    }

    public GameTargetBuilder setAllowedBiomes(Biome... biomes) {
        this.allowedBiomes.addAll(Arrays.asList(biomes));
        return this;
    }

    public GameTargetBuilder addRegularMob(Class<? extends Entity> entityClass, int firstCooldownSeconds, int cooldownSeconds, String spawnMessage, Function<GameDeathArena, Location> spawnLocationGetter) {
        this.regularMobs.add(new RegularMob(entityClass, firstCooldownSeconds, cooldownSeconds, spawnMessage, spawnLocationGetter));
        return this;
    }

    public GameTargetBuilder addRegularMob(Class<? extends Entity> entityClass, int firstCooldownSeconds, int cooldownSeconds, String spawnMessage) {
        this.regularMobs.add(new RegularMob(entityClass, firstCooldownSeconds, cooldownSeconds, spawnMessage, arena -> {
            Location location = arena.getVictim().getLocation();
            int middleX = Randomizer.getInt(-10, 10);
            int middleZ = Randomizer.getInt(-10, 10);
            location.setX(location.getX() + middleX);
            location.setZ(location.getZ() + middleZ);
            int highest = LocationUtils.getHighestBlockYAt(location, false, true);
            location.setY(highest + 1);
            return location;
        }));
        return this;
    }

    public GameTarget build() {
        if (this.night == null) {
            this.night = Randomizer.getBoolean();
        } else if (this.night && this.thunder == null && Randomizer.getInt(1, 100) <= 30) {
            this.night = false;
            this.thunder = true;
        }
        if (this.thunder == null) {
            this.thunder = false;
        }

        return new GameTarget(
                this.world,
                this.night,
                this.thunder,
                this.shouldDie,
                this.durationSeconds,
                this.victimMessage,
                this.opponentsMessage,
                this.filter,
                this.victimKitCooldowns,
                this.opponentsKitCooldowns,
                this.listener,
                this.allowedBiomes.size() == 0 ? null : new ArrayList<>(this.allowedBiomes),
                this.regularMobs
        );
    }
}
