package ru.dymeth.deathgames.util;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.projectiles.ProjectileSource;

public class EntityUtils {
    public static Entity getSourceDamager(EntityDamageEvent event) {
        if (!(event instanceof EntityDamageByEntityEvent)) return null;
        return getSourceDamager(((EntityDamageByEntityEvent) event).getDamager());
    }

    public static Entity getSourceDamager(Entity damager) {
        if (!(damager instanceof Projectile)) return damager;
        ProjectileSource projectileSource = ((Projectile) damager).getShooter();
        if (projectileSource instanceof Entity) return (Entity) projectileSource;
        return damager;
    }
}
