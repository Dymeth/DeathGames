package ru.dymeth.deathgames.util;

import net.minecraft.server.v1_12_R1.NBTTagCompound;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nonnull;

public final class ItemUtil {
    public static String getID(ItemStack stack) {
        return getTagString(stack, "id");
    }

    public static String getTagString(ItemStack stack, String id) {
        net.minecraft.server.v1_12_R1.ItemStack nmsItem = CraftItemStack.asNMSCopy(stack);
        NBTTagCompound tag = getNBTTagCompound(nmsItem);
        return tag.getString(id);
    }

    @Nonnull
    public static NBTTagCompound getNBTTagCompound(@Nonnull net.minecraft.server.v1_12_R1.ItemStack nmsItem) {
        if (nmsItem.getTag() != null) return nmsItem.getTag();
        return new NBTTagCompound();
    }
}
