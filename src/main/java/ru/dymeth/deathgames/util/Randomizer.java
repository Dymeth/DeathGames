package ru.dymeth.deathgames.util;

import javax.annotation.Nullable;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class Randomizer<T> {
    private final Map<T, Float> chances = new HashMap<>();

    public Randomizer<T> addPercentageElement(T element, float chance) {
        if (element == null) throw new RuntimeException("Не указан добавляемый рандомный элемент");
        if (chance <= 0 || chance > 100) throw new RuntimeException("Шанс должен быть дробным числом от 0 до 100");
        this.chances.put(element, chance);
        return this;
    }

    public Set<T> getAllElement() {
        return this.chances.keySet();
    }

    public boolean hasElements() {
        return this.chances.size() > 0;
    }

    public boolean isCorrect() {
        float summ = 0;
        for (float chance : this.chances.values())
            summ += chance;
        if (summ != 100)
            return false;
        Iterator<Map.Entry<T, Float>> iter = this.chances.entrySet().iterator();
        Map.Entry<T, Float> previous = null;
        while (iter.hasNext()) {
            Map.Entry<T, Float> current = iter.next();
            if (previous == null) {
                previous = current;
                continue;
            }
            this.chances.put(current.getKey(), previous.getValue() + current.getValue());
            previous = current;
        }
        return true;
    }

    public T getElement() {
        int random = Randomizer.getInt(1, 100);
        for (Map.Entry<T, Float> element : this.chances.entrySet())
            if (random <= element.getValue())
                return element.getKey();
        throw new RuntimeException("Рандомный элемент не найден");
    }

    private static final Random r = new Random();

    public static boolean getBoolean() {
        return r.nextBoolean();
    }

    public static int getInt(int min, int max) {
        if (min == max) return min; // Optimization
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    public static float getFloat(double min, double max) {
        return (float) getDouble(min, max);
    }

    public static double getDouble(double min, double max) {
        if (min == max) return min; // Optimization
        return (max - min) * r.nextDouble() + min;
    }

    public static short getShort(int min, int max) {
        if (min == max) return (short) min;
        return (short) ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    public static float getFloat() {
        return r.nextFloat();
    }

    public static double getDouble() {
        return r.nextDouble();
    }

    public static <T extends Enum<?>> T getEnum(@Nullable Class<T> clazz) {
        if (clazz == null) return null;
        return clazz.getEnumConstants()[getInt(0, clazz.getEnumConstants().length - 1)];
    }

    public static <T> T getElement(@Nullable List<T> list) {
        if (list == null || list.size() == 0) return null;
        return list.get(getInt(0, list.size() - 1));
    }

    public static <T> T getElement(@Nullable T... objects) {
        if (objects == null || objects.length == 0) return null;
        if (objects.length == 1) return objects[0];
        return objects[getInt(0, objects.length - 1)];
    }

    public static <T> T getElement(@Nullable Collection<T> objects) {
        if (objects == null || objects.size() == 0) return null;
        int num = (int) (Math.random() * objects.size());
        for (T object : objects) if (--num < 0) return object;
        return null;
    }
}
