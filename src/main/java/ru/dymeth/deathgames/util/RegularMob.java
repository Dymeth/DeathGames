package ru.dymeth.deathgames.util;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import ru.dymeth.deathgames.arena.GameDeathArena;

import java.util.function.Function;

public class RegularMob {
    private final Class<? extends Entity> entityClass;
    private final int firstCooldownSeconds;
    private final int cooldownSeconds;
    private final String spawnMessage;
    private final Function<GameDeathArena, Location> spawnLocationGetter;

    public RegularMob(Class<? extends Entity> entityClass, int firstCooldownSeconds, int cooldownSeconds, String spawnMessage, Function<GameDeathArena, Location> spawnLocationGetter) {
        this.entityClass = entityClass;
        this.firstCooldownSeconds = firstCooldownSeconds;
        this.cooldownSeconds = cooldownSeconds;
        this.spawnMessage = spawnMessage;
        this.spawnLocationGetter = spawnLocationGetter;
    }

    public Class<? extends Entity> getEntityClass() {
        return this.entityClass;
    }

    public int getFirstCooldownSeconds() {
        return this.firstCooldownSeconds;
    }

    public int getCooldownSeconds() {
        return this.cooldownSeconds;
    }

    public String getSpawnMessage() {
        return this.spawnMessage;
    }

    public Location getSpawnLocation(GameDeathArena arena) {
        return this.spawnLocationGetter.apply(arena);
    }
}
